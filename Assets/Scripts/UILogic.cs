﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILogic : MonoBehaviour
{
    public Text scoreText;
    public static int score;
    public int treeScore;
    public int guilderScore;
    public Text timeText;
    public float time;
    public ScoreAndTime scoreAndTime;
    public void addGuilderScore()
    {
        score += guilderScore;
    }
    public void addTreeScore()
    {
        score += treeScore;
    }
    private void Update()
    {
        scoreText.text = "Score: " + score;
        timeText.text = "Time: " + (time -=Time.deltaTime);
        if(time <= 0)
        {
            timeText.text = "Time's up";
        }
    }

}
