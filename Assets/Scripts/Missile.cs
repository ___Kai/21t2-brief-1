﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Missile : MonoBehaviour
{
    [SerializeField] private float speed;
    GameObject[] targets;
    Transform target;
    [SerializeField] private GameObject explosionPrefab;
    // Start is called before the first frame update
    void Start()
    {
        targets = GameObject.FindGameObjectsWithTag("Player");
        target = targets[Random.Range(0, targets.Length)].transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
    }
    public void OnTriggerEnter2D(Collider2D Col)
    {
        if(Col.tag == "Player")
        {
            missileExplosion();
            Destroy(Col.gameObject);
        }
        else if(Col.tag == "Trees")
        {
            FindObjectOfType<UILogic>().addGuilderScore();
            missileExplosion();
            Destroy(Col.gameObject);
        }
    }
    private void missileExplosion()
    {
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        FindObjectOfType<AudioManager>().Play("missileExplosion");
        Destroy(gameObject);
    }
}
