﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreAndTime : MonoBehaviour
{
    [SerializeField] public int score;
    public int treeScore; //The score attained by a tree existing in the scene
    public int guilderScore; //the score attained by destorying a guilder within the scene
    [SerializeField] public int time;
    public float maxTime;
    public float timeElapsed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
