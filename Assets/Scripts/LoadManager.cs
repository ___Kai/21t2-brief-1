﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadManager : MonoBehaviour
{
    public UILogic uILogic;
    public int transitionTime;

    // Update is called once per frame
    void Update()
    {
        if(uILogic != null)
        {
            if (uILogic.time <= 0)
            {
                Debug.Log("Dom");
                LoadNextLevel();
            }
        }
    }
    void LoadNextLevel()
    {
       StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }
    IEnumerator LoadLevel(int levelIndex)
    {
        //transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(levelIndex);
    }
}
