﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killAllTrees : MonoBehaviour
{
    [SerializeField] private float selfDestructTime;
    public GameObject poseurTree;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, selfDestructTime);
        Instantiate(poseurTree, transform.position, transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
