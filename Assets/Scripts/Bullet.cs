using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Vector2 target;
    [SerializeField] private float speed;
    [SerializeField] private GameObject plantingATree;
    // Start is called before the first frame update
    void Start()
    {
        target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
    private void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);
        if(transform.position == (Vector3)target)
        {
            Instantiate(plantingATree, transform.position, Quaternion.identity);
            Destroy(gameObject);
            FindObjectOfType<AudioManager>().Play("paintBrush");
            FindObjectOfType<UILogic>().addTreeScore();
        }
    }
}
