﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileSpawner : MonoBehaviour
{
    [SerializeField] private GameObject missilePrefab;
    [SerializeField] private float yPadding;
    private float minX, maxX;
    public int missilesToSpawn;
    public float delayBetweenMissiles;
    // Start is called before the first frame update
    void Start()
    {
        minX = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0)).x;
        maxX = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0)).x;
        float randomX = Random.Range(minX, maxX);
        float yValue = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0)).y;
        //Instantiate(missilePrefab, new Vector3(randomX, yValue + yPadding, 0), Quaternion.identity);
        StartCoroutine(spawnMissiles());
    }

    void Update()
    {

    }
    public IEnumerator spawnMissiles()
    {
        while(missilesToSpawn > 0)
        {
            float randomX = Random.Range(minX, maxX);
            float yValue = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0)).y;
            Instantiate(missilePrefab, new Vector3(randomX, yValue + yPadding, 0), Quaternion.identity);
            yield return new WaitForSeconds(delayBetweenMissiles);
        }
    }
}
