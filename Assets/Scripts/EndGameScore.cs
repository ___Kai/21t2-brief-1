﻿using UnityEngine;
using UnityEngine.UI;

public class EndGameScore : MonoBehaviour
{
    public Text finalScore;
    UILogic uILogic;
    public void OnEnable()
    {
        finalScore.text = "You acheived a score of: \n" + UILogic.score.ToString();
    }
}
